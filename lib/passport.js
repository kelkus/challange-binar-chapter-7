const passport = require ('passport')
const {Strategy: JwtStrategy, ExtractJwt} = require('passport-jwt')
const {user_game} = require('../models')

const options = {
    jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken('authorization'),
    secretOrKey : 'ini rahasia'
}

passport.use(
    new JwtStrategy(options, async(payload,done) => {
        try {
            const user = await user_game.findByPk(payload.id)
            done(null,user)            
        } catch(e){
            done(err,false)
        }
    })
)

module.exports = passport