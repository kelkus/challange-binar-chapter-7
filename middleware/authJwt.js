const jwt = require("jsonwebtoken");

const authenticateUser = (req, res, next) => {
  const token = req.signedCookies.token;
  console.log(token)

  if (!token) {
    throw new Error("Unauthenticated user. Please login first.");
  }

  try {
    const decoded = jwt.verify(token, "ini rahasia");
    req.user = decoded;
    next();
  } catch (error) {
    next(error);
  }
};

module.exports = { authenticateUser };