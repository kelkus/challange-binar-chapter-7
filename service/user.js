const {user_game,user_game_biodata,user_game_histories} = require('../models')
const bcrypt = require('bcrypt')

const createUser = async (userInfo) =>{

    const {name,age,gender,email,password,role} = userInfo

    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);

    const user = await user_game.create ({name,email,password:hash,role})
    
    await user_game_biodata.create ({user_game_id:user.id,age,gender})
    return user
}

const userLogin = async (loginInfo) =>{
    
    const {email,password} = loginInfo
    const user = await user_game.findOne({
        where: {
            email
        }
    })
    //LOGIN TANPA JWT
    // if(!user){
    //     console.log('email tidak terdaftar')
    //     return null
    // }
    // const checkPassword = bcrypt.compareSync(password, user.password)

    // if(!checkPassword){
    //     console.log('pasword salah')
    //     return null        
    // }
    return user
}

const getAllUser = async() =>{
    
    const users = await user_game.findAll({
        include: [user_game_biodata,user_game_histories]
    })
     return users   
}

const deleteUser = async(userId) =>{
    return await user_game.destroy({
        where: {
            id: userId
        }
    })
}

// const hasilGame = async(setHasil) =>{
//     return await setHasil({
//         where:{
//             hasil
//         }
//     })
// }

module.exports = {createUser,userLogin,getAllUser,deleteUser}

