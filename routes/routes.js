const express = require('express')
const routesController = require('../controller/routesController')
const router = express.Router()
const {authenticateUser} = require('../middleware/authJwt')

router.use((req,res,next)=>{
    console.log('routes')
    next()
})

router.get('/home', routesController.home)

router.get('/game', authenticateUser,routesController.game)

module.exports = router