const { render } = require('ejs')
const express = require('express')
const { DATE } = require('sequelize')
const authController = require('../controller/authController')
const router = express.Router()


// route level middleware
router.use((req,res,next)=>{
    console.log('auth')
    next()
})

router.get('/login', authController.loginGet)

router.post('/login', authController.loginPost)

router.get('/registerSucces', authController.registerSuccess)

router.get('/register',authController.register)

router.post('/register', authController.registerPost)

module.exports = router