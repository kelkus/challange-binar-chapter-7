const { response } = require('express')
const express = require('express')
const { DATE } = require('sequelize')
const dashboardController = require('../controller/dashboardController')
const router = express.Router()
const {authenticateUser} = require('../middleware/authJwt')

router.use((req,res,next)=>{
    console.log('dashboard')
    next()
})

router.get('/', authenticateUser,dashboardController.dashboard)

router.get('/game-activity', dashboardController.gameActivity)

router.delete('/game-activity/:userId', dashboardController.delete)

router.get('/manage-user', dashboardController.addUser)

router.post('/manage-user', dashboardController.createUser )

router.get('/scoreboard', dashboardController.scoreBoard)

module.exports = router