const express = require('express')
const app = express()
const port = 5000
var methodOverride = require('method-override')
const cookieParser = require('cookie-parser')

app.use(express.urlencoded({extended: false}))
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(cookieParser("ini rahasia"))

app.get("/", (req,res) =>{
    res.render('page/index')
})

// Home & Game
const router = require("./routes/routes.js")
app.use("/",router)

// login
const authRouter = require("./routes/auth.js")
app.use("/auth",authRouter)

//Dashboard
const dashboardRouter = require("./routes/dashboard.js")
app.use("/dashboard",dashboardRouter)
app.use(methodOverride('_method'))

// internal server error
app.use((err, req, res, next) => {
    res.status(500).json({
        status : "faill",
        errors : err.message
    })
})
app.use((req,res,next)=>{
    res.status(404).json({
        status : "fail",
        errors : "Are you Lost?"
    })
})

app.listen(port,() =>{
    console.log(`web start ${port}`)
}) 