'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt'); 
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game.hasOne(models.user_game_biodata,{
        onDelete: "cascade"
      })
      user_game.hasMany(models.user_game_histories,{
        onDelete: "cascade"
      })
      // define association here
    }
    
    static generateToken = () => {
      const payload = {
        id: this.id,
        email : this.email
      }
      const rahasia = 'ini rahasia'

      const token = jwt.sign(payload, rahasia)

      return token
    }

    checkPassword = (password) => bcrypt.compareSync(password, this.password)

    static authenticate = async ({email, password}) => {
      try{
        const user = await this.findOne({where: {email}})

        if(!user) return Promise.reject('User not found')

        const isPasswordValid = user.checkPassword(password)
        if(!isPasswordValid) Promise.reject('Wrong password')
        return Promise.resolve(user)
      }catch(err){
        return Promise.reject(err)
      }
    }
  }
  user_game.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.ENUM("SuperAdmin","PlayerUser")
  }, {
    sequelize,
    modelName: 'user_game',
    underscored: true,
  });
  return user_game;
};