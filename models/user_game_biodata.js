'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game_biodata.belongsTo(models.user_game,{
        onDelete: "cascade"
      })
      // define association here
    }
  }
  user_game_biodata.init({
    user_game_id: DataTypes.INTEGER,
    age: DataTypes.INTEGER,
    gender: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_game_biodata',
    underscored: true,
  });
  return user_game_biodata;
};