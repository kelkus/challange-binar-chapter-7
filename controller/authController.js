const users = []
const {user_game} = require('../models')
const {createUser, getAllUser,userLogin} = require('../service/user')


function format(user){
    const {id,email} = user
    return {
        id,
        email,
        accessToken :  user_game.generateToken()
    }
}

module.exports = {
    loginGet :(req,res,) => {
        res.render('page/login')
    },
    loginPost :async(req,res,next) => {
   
        const user = await userLogin(req.body)   
        
        if(!user){
            return res.render('page/error')
        }
        try {
            const userAuth = await user_game.authenticate(req.body)
            // res.json(format(userAuth))
           
       } catch (e) {
            res.json(403,{
                message: 'Login Failed'
            })
            res.send(e.message)
        }

        //NOTE
        // Tokennya udah kebikin,tapi gak bisa dipake buat authorization
        
        const token = user_game.generateToken()
        res.cookie("token",token,{ httpOnly: true, signed: true })

        console.log(token)
       
        res.redirect('/')
        // res.send(user)
    },
    registerSuccess : (req,res,) => {
        res.render('page/registerSucces')
    },
    
    register: (req,res,) => {
        res.render('page/register')
    },
    registerPost : async(req,res) => {
   
        const user = await createUser(req.body)
        users.push(user)
        // console.log(users)
        res.redirect('/auth/registerSucces')
    }
}