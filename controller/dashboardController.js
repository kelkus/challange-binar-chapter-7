const users = []
const {createUser, getAllUser, deleteUser} = require('../service/user')

module.exports = {
    dashboard :(req,res,) => {
        res.render('page/dashboard')
    },
    gameActivity : async(req,res,) => {
        const users = await getAllUser()
      
        // console.log(users)
        res.render('page/useractivity',{
            users
        })
    },
    addUser : (req,res,) => {
        res.render('page/adduser')
    },
    createUser : async(req,res) => {
        const user = await createUser(req.body)
        users.push(user)
        res.redirect('/dashboard')
    },
    scoreBoard: async(req,res,) => {
        const users = await getAllUser()
        res.render('page/scoreBoard',{
            users
        })
    },
    delete: async(req,res) => {
        const userId = req.params.userId 
        await deleteUser(userId)
        const users = await getAllUser()
      
        res.render('page/useractivity',{
            users
        })
    }
}